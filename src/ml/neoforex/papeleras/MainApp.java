package ml.neoforex.papeleras;

import ml.neoforex.papeleras.managers.PapeleraManager;
import ml.neoforex.papeleras.objects.Papelera;

public class MainApp {

    public static void main(String[] args) {
        PapeleraManager papeleraManager = new PapeleraManager();

        switch (args[0]) {
            case "afegir":
                Papelera papelera = new Papelera(Long.parseLong(args[1]), Long.parseLong(args[2]),
                        Long.parseLong(args[3]));

                papeleraManager.insertarPapelera(papelera);
                break;
            case "buscar":
                papelera = papeleraManager.obtenerPapelera(Integer.parseInt(args[1]));
                System.out.println(papelera.toString());
                break;
            case "llistar":
                for(Papelera papelera1 : papeleraManager.obtenerPapeleras()){
                    System.out.println(papelera1.toString());
                }
                break;
            case "eliminar":
                papeleraManager.borrarPapelera(Integer.parseInt(args[1]));
                break;
            case "historial":
                System.out.println(papeleraManager.getHistorialManager().obtendreHistorials(Integer.parseInt(args[1])));
                break;
            case "anotar":
                papeleraManager.getHistorialManager().afegirHistorial(Integer.parseInt(args[1]),
                        Integer.parseInt(args[2]), "Anotar");
                break;
        }
    }
}
