package ml.neoforex.papeleras.managers;

import ml.neoforex.papeleras.dao.PapeleraDAO;
import ml.neoforex.papeleras.objects.Papelera;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;

public class PapeleraManager implements PapeleraDAO {

    private final String fileRoute = "papeleras/";
    private final File dataFolder;
    private final HistorialManager historialManager;

    public PapeleraManager() {
        this.dataFolder = new File(fileRoute);
        this.historialManager = new HistorialManager();

        if(!dataFolder.exists())
            dataFolder.mkdirs();
    }

    @Override
    public void insertarPapelera(Papelera papelera) {
        int id = Arrays.stream(dataFolder.listFiles()).filter(e -> e.getName().endsWith(".dat")).
                toArray().length;

        if(existPapelera(papelera)) {
            try {
                throw new Exception("La papelera ya existe.");
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            papelera.setId(id);
            File file = new File(fileRoute.concat(String.valueOf(papelera.getId()).concat(".dat")));
            if (!file.exists()) {
                try {
                    ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream(file));
                    objectOutputStream.writeObject(papelera);
                    objectOutputStream.close();
                    this.historialManager.afegirHistorial(papelera.getId(), papelera.getCap(), "Afegir");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                System.out.println("Existeix ja el fitxer!");
            }
        }
    }

    private boolean existPapelera(Papelera pap) {
        ArrayList<Papelera> papeleras = obtenerPapeleras();

        for(Papelera papelera : papeleras){
            if(papelera.equals(pap))
                return true;
        }

        papeleras.clear();
        return false;
    }

    @Override
    public void borrarPapelera(int id) {
        File file = Arrays.stream(dataFolder.listFiles()).filter(e -> e.getName().equals(id+".dat")).findFirst()
                .orElse(null);

        if (file != null) {
            file.delete();
            this.historialManager.afegirHistorial(id, 0, "Borrar");
        } else {
            System.out.println("No existeix el id introduit.");
        }
    }

    @Override
    public ArrayList<Papelera> obtenerPapeleras() {
        try {
            ArrayList<Papelera> papeleras = new ArrayList<>();
            for(Object files : Arrays.stream(dataFolder.listFiles()).filter(e -> e.getName().endsWith(".dat"))
                    .toArray()) {
                ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream((File)files));
                papeleras.add((Papelera) objectInputStream.readObject());
                objectInputStream.close();
            }
            return papeleras;
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public Papelera obtenerPapelera(int id) {
        File file = Arrays.stream(dataFolder.listFiles()).filter(e -> e.getName().equals(id+".dat")).findFirst()
                .orElse(null);

        try {
            ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(file));
            return (Papelera) objectInputStream.readObject();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }

        System.out.println("No existeix la papelera!");
        return null;
    }

    public HistorialManager getHistorialManager() {
        return historialManager;
    }
}
