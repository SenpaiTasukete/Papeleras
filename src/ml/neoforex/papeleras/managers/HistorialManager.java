package ml.neoforex.papeleras.managers;

import ml.neoforex.papeleras.dao.HistorialDAO;
import ml.neoforex.papeleras.objects.Historial;
import ml.neoforex.papeleras.utils.AppendingObjectOutputStream;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

public class HistorialManager implements HistorialDAO {

    private final String fileRoute = "papeleras/";
    private final File mainDirectory;

    public HistorialManager() {
        this.mainDirectory = new File(fileRoute);

        if(!mainDirectory.exists())
            mainDirectory.mkdirs();
    }

    @Override
    public void afegirHistorial(int idPapelera, double cantitat, String motiu) {
        Historial historial = new Historial(idPapelera, new Date(), motiu, cantitat);
        historial.setId(totalHistorials(idPapelera));

        File historialFile = new File(mainDirectory.getAbsolutePath().concat("/").concat(String.valueOf(idPapelera))
                .concat(".historial"));

        try {
            ObjectOutputStream objectOutputStream;

            if(historialFile.exists())
             objectOutputStream = new AppendingObjectOutputStream(
                    new FileOutputStream(historialFile,true));
            else
                objectOutputStream = new ObjectOutputStream(new FileOutputStream(historialFile,true));

            objectOutputStream.writeObject(historial);
            objectOutputStream.flush();
            objectOutputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public ArrayList<Historial> obtendreHistorials(int idPapelera) {
        ArrayList<Historial> historials = new ArrayList<>();

        File historial = new File(mainDirectory.getAbsolutePath().concat("/").concat(String.valueOf(idPapelera))
                .concat(".historial"));

        if(!historial.exists())
            return historials;

        try {
            ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(historial));
            while(true) {
                try {
                    Historial historial1 = (Historial) objectInputStream.readObject();
                    historials.add(historial1);
                } catch (EOFException e) {
                    break;
                }
            }

            objectInputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return historials;
    }

    @Override
    public int totalHistorials(int idPapelera) {
        return obtendreHistorials(idPapelera).size();
    }

    @Override
    public void borrarHistorial(int idPapelera) {

    }
}
