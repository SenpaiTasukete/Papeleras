package ml.neoforex.papeleras.dao;

import ml.neoforex.papeleras.objects.Historial;

import java.util.ArrayList;

public interface HistorialDAO {

    void afegirHistorial(int idPapelera, double capacitat, String motiu);
    ArrayList<Historial> obtendreHistorials(int idPapelera);
    int totalHistorials(int idPapelera);
    void borrarHistorial(int idPapelera);
}
