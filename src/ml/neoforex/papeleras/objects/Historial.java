package ml.neoforex.papeleras.objects;

import java.io.Serializable;
import java.util.Date;

public class Historial implements Serializable {

    private int id;
    private int idPapelera;
    private Date fechaEditado;
    private String motivo;
    private double capacidad;

    public Historial(int idPapelera, Date fechaEditado, String motivo, double capacidad) {
        this.idPapelera = idPapelera;
        this.fechaEditado = fechaEditado;
        this.motivo = motivo;
        this.capacidad = capacidad;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdPapelera() {
        return idPapelera;
    }

    public void setIdPapelera(int idPapelera) {
        this.idPapelera = idPapelera;
    }

    public Date getFechaEditado() {
        return fechaEditado;
    }

    public void setFechaEditado(Date fechaEditado) {
        this.fechaEditado = fechaEditado;
    }

    public String getMotivo() {
        return motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    public double getCapacidad() {
        return capacidad;
    }

    public void setCapacidad(double capacidad) {
        this.capacidad = capacidad;
    }

    @Override
    public String toString() {
        return "Historial{" +
                "id=" + id +
                ", idPapelera=" + idPapelera +
                ", fechaEditado=" + fechaEditado +
                ", motivo='" + motivo + '\'' +
                ", capacidad=" + capacidad +
                '}';
    }
}
