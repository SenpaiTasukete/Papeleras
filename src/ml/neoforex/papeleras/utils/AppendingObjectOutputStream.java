package ml.neoforex.papeleras.utils;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;

public class AppendingObjectOutputStream  extends ObjectOutputStream {

    protected AppendingObjectOutputStream() throws IOException, SecurityException {
        super();
    }

    public AppendingObjectOutputStream(OutputStream out) throws IOException {
        super(out);
    }

    @Override
    public void writeStreamHeader() throws IOException
    {
        reset();
    }
}