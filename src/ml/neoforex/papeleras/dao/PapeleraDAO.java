package ml.neoforex.papeleras.dao;

import ml.neoforex.papeleras.objects.Papelera;

import java.util.ArrayList;

public interface PapeleraDAO {

    void insertarPapelera(Papelera papelera);
    void borrarPapelera(int id);
    ArrayList<Papelera> obtenerPapeleras();
    Papelera obtenerPapelera(int id);
}
