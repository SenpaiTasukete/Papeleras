package ml.neoforex.papeleras.objects;

import java.io.Serializable;

public class Papelera implements Serializable {

    private int id;
    private long lat;
    private long longitud;
    private long cap;

    public Papelera(long lat, long longitud, long cap) {
        this.lat = lat;
        this.longitud = longitud;
        this.cap = cap;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getLat() {
        return lat;
    }

    public void setLat(long lat) {
        this.lat = lat;
    }

    public long getLongitud() {
        return longitud;
    }

    public void setLongitud(long longitud) {
        this.longitud = longitud;
    }

    public long getCap() {
        return cap;
    }

    public void setCap(long cap) {
        this.cap = cap;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Papelera papelera = (Papelera) o;

        return lat == papelera.lat && longitud == papelera.longitud;
    }

    @Override
    public int hashCode() {
        int result = (int) (lat ^ (lat >>> 32));
        result = 31 * result + (int) (longitud ^ (longitud >>> 32));
        return result;
    }

    @Override
    public String toString() {
        return "Papelera{" +
                "id=" + id +
                ", lat=" + lat +
                ", longitud=" + longitud +
                ", cap=" + cap +
                '}';
    }
}
